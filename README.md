# grapheno
A GPU-accelerated implementation of [PhenoGraph](https://github.com/jacoblevine/PhenoGraph) using [NVIDIA RAPIDS](https://github.com/rapidsai) for fast single-cell phenotyping.

## Installation
Install RAPIDS into new environment:
```bash
conda create -n rapids-22.08 -c rapidsai -c nvidia -c conda-forge  \
    rapids=22.08 python=3.9 cudatoolkit=11.5

```
Activate the new conda environment:
```bash
conda activate rapids-22.08
```

Install grapheno:
```bash
pip install git+https://gitlab.com/eburling/grapheno.git
```

(Optional) Install holoviews for visualization:
```
$ conda install -c pyviz holoviews bokeh
```

The pip/conda requirements files are also included for full disclosure of the environment used to test grapheno.

## Usage
```python
import grapheno

grapheno.generate_dummy_data(n_samples = 5000000,
                             n_features = 20,
                             centers = 30,
                             cluster_std=3.0)

df = grapheno.cluster('dummy_data.csv', 
                      [f'feature{i+1}' for i in range(20)],
                      n_neighbors=30,
                      distributed_knn = True,
                      distributed_graphs = False,
                      min_size=10)
```

## Benchmarking
GPU is orders of magnitude faster than CPU at large data scales. Mean points and error bars are from three replicates:
![benchmarking times](img/benchmark.png)

Modularity is comparable between GPU and CPU implementations. t-SNE embeddings of simulated data colored by cluster label:
![benchmarking tsne](img/tsne.png)

## Disclaimer
This version of `grapheno` has only been tested using RAPIDS 22.08 and may not work for other versions.